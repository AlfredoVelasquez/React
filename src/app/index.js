import ReactDOM from 'react-dom';
import React from 'react';
import Pantalla from '../display';



export default class Calculator extends React.Component {
    state = {
      value: null,
      mostrar: '0',
      historial: [],
      operador: null,
      Mhistorial: [],
      Mhistorialsuma: [],
      siguientenumero: false, 
      Mhistorialdisplay: [],
      
      mc: " ",
      mr: " ", 
      mmas: " ", 
    };

    AC() {
      const {mostrar} = this.state 
      const {Saldo} = this.state
      const {resultado} = this.state
      const {valorRecibido} = this.state

      this.setState({
        mostrar: "0",
        // Saldo: "0",
        // valorRecibido: "0",
        // resultado: "0",
        operador: null,
        value: null
      })
    }

    MC() {
      const {Mhistorial} = this.state
      
      this.setState({
        Mhistorial: [" "]
      })

    }

    // MR() {
    // //  const {resultado} = this.state
    //  const {Mhistorial} = this.state
      
    // //   this.setState ({
    // //     Mhistorial: [resultado]
    // //   })

    //   console.log(Mhistorial)
    // }

    MS() {    
     const {mostrar} = this.state
     const {Mhistorial} = this.state
     const {Mhistorialdisplay} = this.state
     const valorRecibido = parseFloat(mostrar)
    
     this.setState({
       Mhistorial: (Mhistorial.concat([valorRecibido])),
       Mhistorialdisplay: (Mhistorialdisplay.concat(["--"," ",valorRecibido," ","--",<br></br>]))
     })

    //  for (let i = 0; Mhistorial.length ; i++) {
    //   "hola" + String(Mhistorial[i])
      
    // }

     console.log(Mhistorial)
    }

    

    MR() {
      // const {Mhistorial} = this.state 
      // const {Mhistorialsuma} = this.state
      // const {valorRecibido} = this.state
      // const reducer = (accumulator, currentValue) => accumulator + currentValue
      // this.setState({
      //   Mhistorialsuma: (Mhistorialsuma.concat([Mhistorial.reduce(reducer),<br></br>]))
      // })
      // console.log(Mhistorialsuma)
    }

    Mmas() {
      const {Mhistorial} = this.state 
      const {Mhistorialsuma} = this.state
      const {valorRecibido} = this.state
      const reducer = (accumulator, currentValue) => accumulator + currentValue
      this.setState({
        Mhistorialsuma: [...Mhistorialsuma,...[Mhistorial.reduce(reducer),<br></br>]]
      })
      console.log(Mhistorialsuma)
    }

     
    
    C() {
      const { mostrar } = this.state
      const {value} = this.state
      
      this.setState({
        mostrar: mostrar.substring(0, mostrar.length - 1) || '0',
        value: null
      })
    }

    // inputHistory() {
    //   return this.state.historial.map(function(historial) {
    //    return (<li>{historial.id} {historial.Saldo}</li>)
    //     });
    //     }

    resetear() {
      const {historial} = this.state

      this.setState({
        historial: [],

      })
    }
    
    numerador(numero) {
      const { mostrar, siguientenumero } = this.state
      if (siguientenumero) {
        this.setState({
          mostrar: String(numero),
          siguientenumero: false
          //  if (siguientenumero === true)
          //   mostrar: this.state
        })
      } else {
        this.setState({
          // mostrar: mostrar >= 'value' (numero) 
          // numero: mostrar.siguientenumero
          mostrar: mostrar === '0' ? String(numero) : mostrar + numero
        })
      }
      // if (mostrar.length > 11)
      //   this.setState({
      //     mostrar: '0',
      //   })
    }

    coma() {
      const { mostrar } = this.state
      
      if (!(/\./).test(mostrar)) {
        this.setState({
          mostrar: mostrar + '.',
          // mostrar: mostrar + ' , '
          siguientenumero: false
        })
      }
    }
    
    operaciones(nuevooperador) {    
      const { value, mostrar, operador } = this.state
      const valorRecibido = parseFloat(mostrar)
      const Operaciones = {
        '/': (anterior, nuevo) => anterior / nuevo,
        '*': (anterior, nuevo) => anterior * nuevo,
        '+': (anterior, nuevo) => anterior + nuevo,
        '-': (anterior, nuevo) => anterior - nuevo,
        '=': (anterior, nuevo) => anterior 
      }
      
      if (value == null) {
        this.setState({
          value: valorRecibido
        })
      } else if (operador) {
        const Saldo = value || 0
        const resultado = Operaciones[operador](Saldo, valorRecibido)
        const historial = this.state.historial
        // const Mhistorial = this.state.Mhistorial
        // console.log(operador)
        // console.log(Saldo)
        // console.log(operador)
        // console.log(valorRecibido)
        // Saldo: Saldo,
        // operador: operador,
        // valorRecibido: valorRecibido,
        // resultado: resultado,
        // console.log(resultado)
        this.setState({
          value: resultado,
          mostrar: String(resultado),
          historial:(historial.concat([Saldo,"  ",operador,"  ",valorRecibido," = ",resultado,<br></br>])),
          // Mhistorial: (Mhistorial.concat([resultado])),
        })    
        // console.log(resultado)    
      }
      
      
      this.setState({
        siguientenumero: true,
        operador: nuevooperador
      })
    }

    render() {
      const { mostrar } = this.state
      const { historial } = this.state
      const { hola } = this.state
      const { Mhistorial } = this.state
      const { Mhistorialsuma } = this.state
      const { Mhistorialdisplay } = this.state
      
      return (
        <div className="calculator" className="col-12" >
          <div className="row arriba">
            <div className="col-9 dropdown radSeccion">
              <button type="button" className="btn" data-toggle="dropdown">
                <div className="col-1">
                  <div className="rad">RAD</div>
                </div>
              </button>
              <div className="dropdown-menu dropdown-menu-right rad">
                <a className="dropdown-item rad"><span className="decoracion">|-Memoria-|</span> <br></br><br></br>Resultados:<br></br>{Mhistorialdisplay}<br></br> Sumas:<br></br>{ Mhistorialsuma }  </a>
                {/* <a className="dropdown-item rad-2" onClick={() => this.resetear()}>reset</a> */}
              </div>
          </div>
            <div className="col-3 dropdown">
              <button type="button" className="btn" data-toggle="dropdown">
                <div className="col-1">
                  <div className="historial">…</div>
                </div>
              </button>
              <div className="dropdown-menu dropdown-menu-right">
                <a className="dropdown-item">HISTORIAL: <br/>{historial}</a>
                <a className="dropdown-item-2" onClick={() => this.resetear()}><span className="resete">reset</span></a>
              </div>
            </div>
          </div>
          <div className="achicate" >
            <Pantalla value={mostrar}/>
            {/* <div>{Mhistorial}</div>
            <div>{Mhistorialsuma}</div> */}
          </div>
          <div className="row">
            <div className="col-9 tablero">
              <div className="row numeradorSeccion">
                <button className="col-4 numeradores" onClick={() => this.numerador(7)}>7</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(8)}>8</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(9)}>9</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(4)}>4</button>                
                <button className="col-4 numeradores" onClick={() => this.numerador(5)}>5</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(6)}>6</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(1)}>1</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(2)}>2</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(3)}>3</button>                
                <button className="col-4 numeradores" onClick={() => this.coma()}>●</button>
                <button className="col-4 numeradores" onClick={() => this.numerador(0)}>0</button>
                <button className="col-4 numeradores" onClick={() => this.operaciones('=')}>=</button>
              </div>
            </div>
            <div className="col-2 operadorSeccion">
              <button className="operador" onClick={() => this.C('C')}>C</button>
              <button className="operador" onClick={() => this.operaciones('/')}>÷</button>
              <button className="operador" onClick={() => this.operaciones('*')}>×</button>
              <button className="operador" onClick={() => this.operaciones('-')}>−</button>
              <button className="operador" onClick={() => this.operaciones('+')}>+</button>
            </div>
            <div className="col-1 dropdown secreto">
              <button type="button" className="btn secreto" data-toggle="dropdown">
                <div className="col-1 barra-azul">
                  <button className="botoncito">‹</button>
                </div>
              </button>
                <div className="col-12 dropdown-menu dropdown-menu-right nuevo">
                  <button className="dropdown-item-4">></button>
                  <button className="col-6 dropdown-item-3" onClick={() => this.AC()}>AC</button>
                  <button className="col-6 dropdown-item-3" onClick={() => this.MC()}>MC</button>
                  <button className="col-6 dropdown-item-3" onClick={() => this.Mmas()}>M+</button>
                  <button className="col-6 dropdown-item-3" onClick={() => this.MS()}>MS</button>
              </div>
            </div>
          </div>
        </div>
      )
    }
  }
  
