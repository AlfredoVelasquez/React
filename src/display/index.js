import ReactDOM from 'react-dom';
import React from 'react';

//  Esto la verdad no entiendo muy bien como funciona, pero esta semana ha sido 
//  muy estresante con temas familiares ,por lo cual mi tiempo estuvo extra limitado,
//  asi que busque en internet hasta que consegui esta forma de poner la coma, solo para 
//  navegadores.

export default class Pantalla extends React.Component {
    render() {
      const { value, ...props } = this.props
      
      const language = navigator.language || 'en-US'
      let formattedValue = parseFloat(value).toLocaleString(language, {
        useGrouping: true,
        maximumFractionDigits: 6
      })
      
      // Add back missing .0 in e.g. 12.0
      const match = value.match(/\.\d*?(0*)$/)
      
      if (match)
        formattedValue += (/[1-9]/).test(match[0]) ? match[1] : match[0]
      
      return (
        <div {...props} className="calculator-display">
          <div className="centrado">{formattedValue}</div>
        </div>
      )
    }
  }
  